(ns mrsudoku.utils-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [mrsudoku.utils :refer [reduce-stop]]))

(deftest reduce-stop-test
  (is (nil? (reduce-stop conj [] nil)))
  (is (nil? (reduce-stop conj [] [1 2 3 nil 4])))
  (is (nil? (reduce-stop #(if (> %1 10) nil (+ %1 %2)) 0 [1 2 3 4 5 6 7])))
  (is (= 55 (reduce-stop + 0 (range 11)))))
