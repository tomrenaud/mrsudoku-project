(ns mrsudoku.matching-test
    (:require [cljs.test :refer-macros [deftest testing is]]
              [mrsudoku.model.matching :refer [augment max-matching
                                               complete-matching?
                                               graph-with-matching]]))

(def g {:x1 #{1 4 43}
        :x2 #{1}
        :x3 #{4}})

(deftest augment-test
  (is (= (augment g :x1 #{} {}) ;Vrai appel 1
         [true, #{1}, {1 :x1}]))
  (is (= (augment g :x1 #{1} {1 :x1}) ;Récursif 2.1
       [true, #{1 4}, {1 :x1, 4 :x1}]))
  (is (= (augment g :x2 #{} {1 :x1}) ;Vrai appel 2
       [true, #{1 4}, {1 :x2, 4 :x1}]))
  (is (= (augment g :x1 #{4} {1 :x2, 4 :x1}) ;Récursif 3.1
         [true, #{1 4 43}, {1 :x2, 4 :x1, 43 :x1}]))
  (is (= (augment g :x2 #{1 4} {1 :x2, 4 :x1}) ;Récursif 3.2
       [false, #{1 4}, {1 :x2, 4 :x1}]))
  (is (= (augment g :x3 #{} {1 :x2, 4 :x1}) ;Vrai appel
        [true, #{1 4 43}, {1 :x2, 4 :x3, 43 :x1}])))

(deftest max-matching-test
  (is (= (max-matching g)
         {1 :x2, 4 :x3, 43 :x1})))

(deftest complete-matching-test
  (is (complete-matching? #{:x1 :x2 :x3} (max-matching g))))

(deftest graph-with-matching-test
  (is (= (graph-with-matching g (max-matching g))
         {:x1 #{1 4}
          1 #{:x2}
          4 #{:x3}
          43 #{:x1}})))
