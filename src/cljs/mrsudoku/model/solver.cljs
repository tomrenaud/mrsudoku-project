(ns mrsudoku.model.solver
  (:require [mrsudoku.model.alldiff :as alldiff]
            [mrsudoku.model.grid :as g]
            [mrsudoku.utils :refer [reduce-stop]]
            [clojure.set :as set]))

(defn cell-to-domain
  "Takes a single cell and creates its domain"
  [cell]
  (case (:status cell)
    (:init :set :solved) #{(:value cell)}
    :possible (:value cell)
    (:empty :conflict) (into #{} (range 1 10))))

(defn domain-to-cell
  "Takes the original grid and a single cells
  [coord domain]-tuple, and changes its status.
  If there's no possibility at all, returns nil."
  [grid cell]
  (let [[[x y] dom] cell
        n (count dom)
        old (g/cell grid x y)]
    (cond
      (contains? #{:init :set :solved} (:status old)) grid
      (zero? n) nil
      (= 1 n) (g/change-cell grid x y {:status :solved, :value (first dom)})
      (contains? #{:empty :conflict} (:status old)) (g/change-cell grid x y {:status :possible, :value dom})
      (= (:status old) :possible)
      (let [newDom (set/intersection dom (:value old))]
        (case (count newDom)
          0 nil
          1 (g/change-cell grid x y {:status :solved, :value (first newDom)})
          (g/change-cell grid x y {:status :possible, :value newDom}))))))

(defn cols-to-doms
  "Takes the entire grid and produces the domains of all cells, col by col"
  [grid]
  (for [i (range 1 10)]
     (into {} (for [j (range 1 10)]
                [[i j] (cell-to-domain (g/cell grid i j))]))))

(defn rows-to-doms
  "Takes the entire grid and produces the domains of all cells, row by row"
  [grid]
  (for [i (range 1 10)]
   (into {} (for [j (range 1 10)]
             [[j i] (cell-to-domain (g/cell grid j i))]))))

(defn blocks-to-doms
  "Takes the entire grid and produces the domains of all cells, block by block"
  [grid]
  (for [i (range 9)]
   (into {} (for [j (range 9)]
              (let [i' (+ (rem (* i 3) 9) (quot j 3) 1)
                    j' (+ (rem j 3) (* (quot i 3) 3) 1)]
                  [[i' j'] (cell-to-domain (g/cell grid i' j'))])))))

(defn apply-alldiff
 "Slices the grid with given 'x-to-doms' function, applies alldiff
  algorithm to the slices, merges all the results together,
  and finally puts the results into the original grid."
 [grid x-to-doms]
 (->> grid
      (x-to-doms)
      (map alldiff/alldiff)
      (reduce-stop merge {})
      (reduce-stop domain-to-cell grid)))
(comment (reduce-stop domain-to-cell grid
          (reduce-stop merge {}
            (map alldiff/alldiff (x-to-doms grid)))))

(defn choose-var
  "Choose the cell in given grid with the most possible values."
  [grid]
  (g/reduce-grid (fn [[[x y] acc] cx cy cell]
                   (if (or (not= (:status cell) :possible)
                           (<= (count (:value cell)) (count (:value acc))))
                     [[x y] acc]
                     [[cx cy] cell]))
      [[-1 -1] {}] grid))

(defn solve
  "Solve the sudoku `grid` by returning a fully solved grid,
   or `nil` if the solver fails."
  [grid]
  ;(println "Début du solve !")
  (when-let [newGrid (-> grid
                         (apply-alldiff cols-to-doms)
                         (apply-alldiff rows-to-doms)
                         (apply-alldiff blocks-to-doms))]
    ;(do (println "Alldiff terminés, on passe à la suite")
    (if (g/win? newGrid)
      newGrid ;Victoire
      (let [[[x y] cell] (choose-var newGrid)]
        ;(print "On modifie la cellule ("x","y") qui a"(count (:value cell))"valeurs possibles")
        (loop [values (:value cell)]
          (if (seq values)
            ;(do (println "Valeur choisie :" (first values))
            (if-let [grid' (solve (g/change-cell newGrid x y
                                    {:status :solved, :value (first values)}))]
              grid' ;Succès en profondeur, on le remonte
              (recur (rest values))) ;On passe à la valeur suivante
            nil)))))) ;On a tout essayé, problème insatisfiable

;Algo:
;on prend les domaines de toutes les colonnes
;on les donne à alldiff
;on les refusionne et les mets dans la grille
;pareil pour les lignes
;pareil pour les blocs
;On devrait en avoir fini avec alldiff normalement
;Le sudoku est peut-être déjà terminé.
;Si c'est pas le cas on affecte une valeur à une variable, et on recommence tout
;si c'est insatisfiable avec cette valeur, on teste la suivante, et ainsi de suite
