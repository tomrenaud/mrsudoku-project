(ns mrsudoku.model.matching
    (:require [mrsudoku.model.graph :as gr]))

(defn augment
  "Tries to augment the match with the supplied visited nodes.
   Returns the reverted edges of the complete maximal matching."
  [graph src visited match]
  (loop [dests (get graph src), visited visited]
    (if (seq dests)
      (if (visited (first dests))
        (recur (rest dests) visited)
        (if-let [old-src (get match (first dests))]
          (let [[found, visited', match']
                (augment graph old-src (conj visited (first dests)) match)]
            (if found
              [true, visited', (assoc match' (first dests) src)]
              (recur (rest dests) visited')))
          ;La valeur qu'on veut n'est pas dans le match
          [true, (conj visited (first dests)), (assoc match (first dests) src)]))
      ;Aucune valeur disponible
     [false, visited, match])))

(defn max-matching
 "Computes the maximal matching of the given graph."
 [graph]
 (reduce (fn [m node] (last (augment graph node #{} m)))
     {} (gr/vars-of graph)))
 ; Explication du reduce en version loop
 ;(loop [nodes (gr/vars-of graph), match {}]
 ;   (if (seq nodes)
 ;  (let [[_, _, match'] (augment graph (first nodes) #{} match)]
 ;      (recur (rest nodes) match'))
 ;    match)))

(defn complete-matching? [vars match]
  (= (count vars) (count match)))

(defn graph-with-matching
  "Reverses the good arrows and deletes their opposites.
  Complicated stuff."
  [graph match]
  (reduce (fn [mgraph [src dest]]
            (-> mgraph
              (gr/add-edge src dest)
              (gr/remove-edge dest src))) graph match))
