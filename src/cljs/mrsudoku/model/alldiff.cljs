(ns mrsudoku.model.alldiff
    (:require [mrsudoku.model.matching :as matching]
              [mrsudoku.model.scc :as scc]
              [mrsudoku.model.graph :as gr]
              [clojure.set :as set]))

(defn value-known-by
  "Creates the set of variables who 'know' value."
  [doms value]
  (reduce (fn [res [v values]]
            (if (contains? values value)
              (conj res v)
              res))
          #{} doms))

(defn add-value
  "Adds given value to all variables vs in doms"
  [doms vs value]
  (into doms (map
               (fn [var] [var, (conj (get doms var #{}) value)])
               vs)))

(defn access
  "Part one of the alldifferent algorithm.
   Uses the scc to trim down variable domains."
  [doms scc]
  (let [scc-doms (scc/doms-from-scc (gr/vars-of doms) scc)
        isolated-vars (scc/isolated-variables (gr/vars-of doms) scc)
        isolated-vals (scc/isolated-values (gr/vars-of doms) scc)]
    (reduce (fn [doms' value]
              (add-value
                  doms'
                  (value-known-by (select-keys doms isolated-vars) value)
                  value))
            scc-doms
            isolated-vals)))

(defn alldiff-comp
  "Part two of the alldifferent algorithm.
   Uses the singletons in graph to trim down domains,
   until nothing can be done without guesswork."
  ([graph] (alldiff-comp graph {}))
  ([graph taken]
   (let [taken' (into taken (filter #(= (count (second %)) 1) graph))]
     (if (not= (count taken) (count taken'))
       (loop [s taken', graph graph]
         (if (seq s)
           (let [v (first (second (first s)))
                 graph' (conj (reduce
                               #(gr/remove-edge %1 %2 v)
                               graph (value-known-by graph v))
                         (first s))]
             (if (not= (count graph) (count graph'))
               nil ;Si on a supprimé un noeud il y a un problème
               (recur (rest s) graph')))
          (alldiff-comp graph taken')))
       graph))))

(defn alldiff
  "Simplifies all domains in an all-different CSP.
  Returns nil if not satisfiable."
  [domains]
  (let [match (matching/max-matching domains)]
    (if (matching/complete-matching? domains match)
      (let [scc (scc/compute-scc (matching/graph-with-matching domains match))]
        (alldiff-comp (access domains scc)))
      ;Unsatisfiable
      nil)))
