(ns mrsudoku.model.graph)

(defn vars-of
  "Returns a set of all variables in given graph."
  [graph]
  (into #{} (keys graph)))

(defn add-vertex
  "Adds an unlinked vertex to the graph.
  Does nothing if already present"
  [graph vert]
  (if (contains? graph vert)
    graph
    (assoc graph vert #{})))

(defn add-edge
  "Adds the a->b edge to the supplied graph"
  [graph a b]
  (update graph a #(conj (or % #{}) b)))

(defn remove-edge
  "Removes the a->b edge in graph g"
  [g a b]
  (if (= (get g a) #{b})
    (dissoc g a)
    (update g a #(disj % b))))

(defn dfs-pre
  "Depth first search with a given function.
   Prefix function"
  ([graph vert f init] (first (dfs-pre graph vert f init #{})))
  ([graph vert f init visited]
   (if (visited vert)
     [init visited]
     (loop [verts (get graph vert), res (f init vert), visited (conj visited vert)]
       (if (seq verts)
         (let [[res', visited'] (dfs-pre graph (first verts) f res visited)]
           (recur (rest verts) res' visited'))
         ;This node doesn't have anymore children
         [res, visited])))))

(defn dfs-post
  "Depth first search with a given function.
   Postfix function"
  ([graph vert f init] (first (dfs-post graph vert f init #{})))
  ([graph vert f init visited]
   (if (visited vert)
     [init visited]
     (loop [verts (get graph vert), res init, visited (conj visited vert)]
       (if (seq verts)
         (let [[res', visited'] (dfs-post graph (first verts) f res visited)]
           (recur (rest verts) res' visited'))
         ;This node doesn't have anymore children
         [(f res vert), visited])))))

(defn transpose
  "Transpose given graph, that is, reverse all the arrows."
  [graph]
  (let [edges (for [left (keys graph), right (get graph left)]
                [right left])]
    (reduce (fn [ngraph [a b]] (add-edge (add-vertex ngraph b) a b)) {} edges)))

(defn dfs-stack
  "Builds the stack from all dfs-post calls in given graph."
  [graph]
  (loop [verts (vars-of graph), stack (), visited #{}]
    (if (seq verts)
      (let [[stack' visited'] (dfs-post graph (first verts) conj stack visited)]
        (recur (rest verts) stack' visited'))
      ;No more children
      stack)))
