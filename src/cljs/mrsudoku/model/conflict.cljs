(ns mrsudoku.model.conflict
  (:require [clojure.set :as set]
            [mrsudoku.model.grid :as g]))

;; ========================================================================
;; Fichier à compléter : il faut utiliser les fonctions du namespace grid.
;; et regarder également les tests dans `test/cljs/mrsudoku/conflict_test.cljs`
;; et bien sûr `test/cljs/mrsudoku/grid_test.cljs`  (et les autres tests ...)
;; ========================================================================

(defn values
  "Return the set of values of a vector or grid `cells`."
  [cells]
  (reduce (fn [res cell]
            (if-let [v (g/cell-value cell)]
              (conj res v)
              res)) #{} cells))

(defn values-except
  "Return the set of values of a vector of cells, except the `except`-th."
  [cells except]
  {:pre [(<= 1 except (count cells))]}
  (loop [s cells, i 1, res #{}]
    (if (seq s)
      (recur (rest s) (inc i)
        (if (or (= i except) (not (g/cell-value (first s))))
          res
          (conj res (g/cell-value (first s)))))
      res)))

(defn mk-conflict [kind cx cy value]
  {:status :conflict
   :kind kind
   :value value})

(defn merge-conflict-kind
  "Merges two types of conflict on a given cell."
  [kind1 kind2]
  (if (set? kind1)
    (if (set? kind2)
      (clojure.set/union kind1 kind2)
      (conj kind1 kind2))
    (if (set? kind2)
      (conj kind2 kind1)
      (if (= kind1 kind2)
        kind1
        #{kind1 kind2}))))

(defn merge-conflict
  "Merges two conflicts on the same cell."
  [conflict1 conflict2]
  {:status :conflict
    :kind (merge-conflict-kind (:kind conflict1) (:kind conflict2))
    :value (:value conflict1)}) ; WARNING: 1 par défaut, supposition hasardeuse

(defn merge-conflicts [& conflicts]
  (apply (partial merge-with merge-conflict) conflicts))

(defn update-conflicts
  [conflict-kind cx cy value conflicts]
  (if-let [conflict (get conflicts [cx, cy])]
    (assoc conflicts [cx, cy] (mk-conflict (merge-conflict-kind conflict-kind (:kind conflict))
                                           cx cy value))
    (assoc conflicts [cx, cy] (mk-conflict conflict-kind cx cy value))))

(defn conflict-value [values except cell]
  (when-let [value (g/cell-value cell)]
    (when (and (not= (:status cell) :init)
               (contains? (values-except values except) value))
      value)))

(defn col-conflicts
  "Returns a map of conflicts in a `col`."
  [col cx]
  (loop [cy 1, cells col, conflicts {}]
    (if (seq cells)
      (let [cell (first cells)]
        (if-let [value (conflict-value col cy cell)]
          (recur (inc cy) (rest cells) (update-conflicts :col cx cy value conflicts))
          (recur (inc cy) (rest cells) conflicts)))
      ;; no more cells
      conflicts)))

(defn cols-conflicts
  "Returns a map of conflicts in all cols of `grid`"
  [grid]
  (reduce merge-conflicts {}
    (map (fn [c] (col-conflicts (g/col grid c) c)) (range 1 10))))

(defn row-conflicts
  "Returns a map of conflicts in a `row`."
  [row cy]
  (loop [cx 1, cells row, conflicts {}]
    (if (seq cells)
      (let [cell (first cells)]
        (if-let [value (conflict-value row cx cell)]
          (recur (inc cx) (rest cells) (update-conflicts :row cx cy value conflicts))
          (recur (inc cx) (rest cells) conflicts)))
      ;; no more cells
      conflicts)))

(defn rows-conflicts
  "Returns a map of conflicts in all rows of `grid`"
  [grid]
  (reduce merge-conflicts {}
    (map (fn [c] (row-conflicts (g/row grid c) c)) (range 1 10))))
    ; WARNING: copié collé du col !

(defn block-conflicts
  [block b]
  (g/reduce-block (fn [conflicts index cx cy cell]
                    (if-let [value (conflict-value block index cell)]
                      (update-conflicts :block cx cy value conflicts)
                      conflicts)) {} block b))

(defn blocks-conflicts
  [grid]
  (reduce merge-conflicts {}
          (map (fn [b] (block-conflicts (g/block grid b) b)) (range 1 10))))

(defn grid-conflicts
  "Compute all conflicts in the Sudoku grid."
  [grid]
  (println "compute conflicts")
  (println (g/grid->str grid))
  (merge-conflicts (rows-conflicts grid)
                   (cols-conflicts grid)
                   (blocks-conflicts grid)))
