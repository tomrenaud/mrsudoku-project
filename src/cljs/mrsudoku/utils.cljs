(ns mrsudoku.utils)

(defn concatv
  "Concatenate vectors."
  ([] [])
  ([v] v)
  ([v1 v2] (into v1 v2))
  ([v1 v2 & more]
   (into v1 (apply concat (cons v2 more)))))

(defn reduce-stop
  "Like reduce, but stops (returns nil) if nil/false
  is encountered in coll or as a result of calling f."
  [f init coll]
  (if (nil? coll)
    nil
    (reduce #(if (nil? %2)
               (reduced nil)
               (let [called (f %1 %2)]
                 (if (nil? called)
                   (reduced nil)
                   called)))
          init coll)))
