(ns mrsudoku.alldiff-test
  (:require [cljs.test :refer-macros [deftest testing is]]
    [mrsudoku.model.grid :as g]
    [mrsudoku.model.solver :refer [cols-to-doms]]
    [mrsudoku.model.alldiff :refer [value-known-by add-value
                                    access alldiff-comp alldiff]]))

(def ^:private sudoku-grid @#'g/sudoku-grid)

(def graph1 {:A #{:B, :C, :F}
             :B #{:C}
             :C #{:D}
             :D #{:E}
             :E #{:C}
             :F #{:C, :G, :H}
             :G #{:H, :I}
             :H #{:F, :I}
             :I #{}})

(def graph2 {:v1 #{1 2 3}
             :v2 #{1 2 4 5}
             :v3 #{4 5 6}
             :v4 #{4 5 6}
             :v5 #{4 5 6}})

(deftest value-known-by-test
  (is (= (value-known-by graph1 :C)
         #{:A :B :E :F}))
  (is (= (value-known-by graph2 6)
         #{:v3 :v4 :v5})))

(deftest add-value-test
  (is (= (add-value graph1 [:B, :I] :T)
         {:A #{:B, :C, :F}
          :B #{:C, :T}
          :C #{:D}
          :D #{:E}
          :E #{:C}
          :F #{:C, :G, :H}
          :G #{:H, :I}
          :H #{:F, :I}
          :I #{:T}}))
  (is (= (add-value graph2 [:v3, :v4] 3)
         {:v1 #{1 2 3}
          :v2 #{1 2 4 5}
          :v3 #{3 4 5 6}
          :v4 #{3 4 5 6}
          :v5 #{4 5 6}})))

(deftest access-test
  (is (= (access graph2 [#{:v1} #{:v2} #{1} #{2} #{3} #{:v3 :v4 :v5 4 5 6}])
         {:v1 #{1 2 3}
          :v2 #{1 2}
          :v3 #{4 5 6}
          :v4 #{4 5 6}
          :v5 #{4 5 6}}))
  (is (= (access {[1 1] #{5}
                  [1 2] #{6}
                  [1 3] #{1 2 3 4 5 6 7 8 9}
                  [1 4] #{8}
                  [1 5] #{4}
                  [1 6] #{7}
                  [1 7] #{1 2 3 4 5 6 7 8 9}
                  [1 8] #{1 2 3 4 5 6 7 8 9}
                  [1 9] #{1 2 3 4 5 6 7 8 9}}
           [#{[1 3] 2 [1 9] 3 [1 8] 9 [1 7] 1}
            #{8} #{[1 4]} #{7} #{[1 6]} #{6} #{[1 2]} #{5} #{[1 1]} #{4} #{[1 5]}])
         {[1 1] #{5}
          [1 2] #{6}
          [1 3] #{1 2 3 9}
          [1 4] #{8}
          [1 5] #{4}
          [1 6] #{7}
          [1 7] #{1 2 3 9}
          [1 8] #{1 2 3 9}
          [1 9] #{1 2 3 9}})))

(deftest alldiff-comp-test
  (is (= (alldiff-comp {:a #{1}
                        :b #{1 2}
                        :c #{2 3}
                        :d #{3 4}
                        :e #{4 5}
                        :f #{5 6}
                        :g #{6 7}
                        :h #{7 8}
                        :i #{8 9}})
         {:a #{1}
          :b #{2}
          :c #{3}
          :d #{4}
          :e #{5}
          :f #{6}
          :g #{7}
          :h #{8}
          :i #{9}})))

(deftest alldiff-test
  (is (= (alldiff graph2)
         {:v1 #{1 2 3}
          :v2 #{1 2}
          :v3 #{4 5 6}
          :v4 #{4 5 6}
          :v5 #{4 5 6}}))
  (is (= (alldiff {:a #{1}
                   :b #{1 2}
                   :c #{2 3}
                   :d #{3 4}
                   :e #{4 5}
                   :f #{5 6}
                   :g #{6 7}
                   :h #{7 8}
                   :i #{8 9}})
         {:a #{1}
          :b #{2}
          :c #{3}
          :d #{4}
          :e #{5}
          :f #{6}
          :g #{7}
          :h #{8}
          :i #{9}}))
  (is (= (alldiff (first (cols-to-doms sudoku-grid)))
         {[1 1] #{5}
          [1 2] #{6}
          [1 3] #{1 2 3 9}
          [1 4] #{8}
          [1 5] #{4}
          [1 6] #{7}
          [1 7] #{1 2 3 9}
          [1 8] #{1 2 3 9}
          [1 9] #{1 2 3 9}})))
