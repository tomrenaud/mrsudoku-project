(ns mrsudoku.scc-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [mrsudoku.model.matching :as m]
            [mrsudoku.model.scc :refer [compute-scc doms-from-sccomp doms-from-scc
                                        isolated-values isolated-variables]]))

(def graph1 {:A #{:B, :C, :F}
             :B #{:C}
             :C #{:D}
             :D #{:E}
             :E #{:C}
             :F #{:C, :G, :H}
             :G #{:H, :I}
             :H #{:F, :I}
             :I #{}})

(def graph2 {:v1 #{1 2 3}
             :v2 #{1 2 4 5}
             :v3 #{4 5 6}
             :v4 #{4 5 6}
             :v5 #{4 5 6}})

(deftest compute-scc-test
  (let [g1 {:v1 #{1 2}
            :v2 #{1 2}}]
    (is (= (compute-scc (m/graph-with-matching g1 (m/max-matching g1)))
           [#{:v1 :v2 1 2}])))
  (is (= (compute-scc (m/graph-with-matching graph2 (m/max-matching graph2)))
         [#{3} #{:v1} #{1} #{:v2} #{2} #{:v3 :v4 :v5 4 5 6}]))
  (let [g2 {[1 1] #{5}
            [1 2] #{6}
            [1 3] #{1 2 3 4 5 6 7 8 9}
            [1 4] #{8}
            [1 5] #{4}
            [1 6] #{7}
            [1 7] #{1 2 3 4 5 6 7 8 9}
            [1 8] #{1 2 3 4 5 6 7 8 9}
            [1 9] #{1 2 3 4 5 6 7 8 9}}]
      (is (= (compute-scc (m/graph-with-matching g2 (m/max-matching g2)))
           [#{[1 9] 3 [1 8] 2 [1 7] 9 [1 3] 1}
            #{8} #{[1 4]} #{5} #{[1 1]} #{6} #{[1 2]} #{4} #{[1 5]} #{7} #{[1 6]}]))))

(deftest doms-from-sccomp-test
  (is (= (doms-from-sccomp #{:v1 :v2} #{1 2 :v1 :v2})
         {:v1 #{1 2}
          :v2 #{1 2}}))
  (is (= (doms-from-sccomp #{:v1 :v2 :v3 :v4} #{3 1 2 :v1 :v4})
         {:v1 #{1 2 3}
          :v4 #{1 2 3}})))

(deftest doms-from-scc-test
  (is (= (doms-from-scc #{:v1 :v2 :v3 :v4}
           [#{:v1} #{1} #{:v2} #{2} #{:v3 :v4 3 4}])
         {:v1 #{}
          :v2 #{}
          :v3 #{3 4}
          :v4 #{3 4}}))
  (is (= (doms-from-scc #{:v1 :v2 :v3 :v4 :v5}
           (compute-scc (m/graph-with-matching graph2 (m/max-matching graph2))))
         {:v1 #{}
          :v2 #{}
          :v3 #{4 5 6}
          :v4 #{4 5 6}
          :v5 #{4 5 6}}))
  (is (= (doms-from-scc #{[1 1] [1 2] [1 3]
                          [1 4] [1 5] [1 6]
                          [1 7] [1 8] [1 9]}
                         [#{[1 3] 2 [1 9] 3 [1 8] 9 [1 7] 1}
                          #{8} #{[1 4]} #{7} #{[1 6]} #{6} #{[1 2]} #{5} #{[1 1]} #{4} #{[1 5]}])
         {[1 1] #{}
          [1 2] #{}
          [1 3] #{1 2 3 9}
          [1 4] #{}
          [1 5] #{}
          [1 6] #{}
          [1 7] #{1 2 3 9}
          [1 8] #{1 2 3 9}
          [1 9] #{1 2 3 9}})))

(deftest isolated-values-test
  (is (= (isolated-values #{:v1 :v2 :v3}
           [#{:v1} #{1} #{:v2 2 :v3 3}])
         #{1}))
  (is (= (isolated-values #{:v1 :v2 :v3 :v4 :v5}
           [#{3} #{:v1} #{:v2} #{1} #{2} #{4 5 6 :v3 :v4 :v5}])
         #{1 2 3}))
  (is (= (isolated-values #{:v1 :v2 :v3 :v4 :v5}
           (compute-scc (m/graph-with-matching graph2 (m/max-matching graph2))))
         #{1 2 3}))
  (is (= (isolated-values #{[1 1] [1 2] [1 3]
                            [1 4] [1 5] [1 6]
                            [1 7] [1 8] [1 9]}
                         [#{[1 3] 2 [1 9] 3 [1 8] 9 [1 7] 1} #{8} #{[1 4]} #{7} #{[1 6]} #{6} #{[1 2]} #{5} #{[1 1]} #{4} #{[1 5]}])
         #{4 5 6 7 8})))

(deftest isolated-variables-test
  (is (= (isolated-variables #{:v1 :v2 :v3}
           [#{:v1} #{1} #{:v2 2 :v3 3}])
         #{:v1}))
  (is (= (isolated-variables #{:v1 :v2 :v3 :v4 :v5}
           [#{3} #{:v1} #{:v2} #{1} #{2} #{4 5 6 :v3 :v4 :v5}])
         #{:v1 :v2}))
  (is (= (isolated-variables #{:v1 :v2 :v3 :v4 :v5}
           (compute-scc (m/graph-with-matching graph2 (m/max-matching graph2))))
         #{:v1 :v2}))
  (is (= (isolated-variables #{[1 1] [1 2] [1 3]
                               [1 4] [1 5] [1 6]
                               [1 7] [1 8] [1 9]}
                         [#{[1 3] 2 [1 9] 3 [1 8] 9 [1 7] 1} #{8} #{[1 4]} #{7} #{[1 6]} #{6} #{[1 2]} #{5} #{[1 1]} #{4} #{[1 5]}])
         #{[1 1] [1 2] [1 4] [1 5] [1 6]})))
