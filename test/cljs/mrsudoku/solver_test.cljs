(ns mrsudoku.solver-test
  (:require [cljs.test :refer-macros [deftest testing is]]
            [mrsudoku.model.grid :as g :refer [mk-cell]]
            [mrsudoku.model.solver :refer [cell-to-domain domain-to-cell
                                                 cols-to-doms rows-to-doms
                                                 blocks-to-doms apply-alldiff
                                                 choose-var solve]]))
(def ^:private sudoku-grid @#'g/sudoku-grid)

(def ^:private solved-grid @#'g/solved-grid)

(def ^:private partial-grid @#'g/partial-grid)

(deftest cell-to-domain-test
  (is (= (cell-to-domain {:status :init, :value 4})
         (cell-to-domain {:status :set, :value 4})
         (cell-to-domain {:status :solved, :value 4})
         #{4}))
  (is (= (cell-to-domain {:status :empty})
         (cell-to-domain {:status :conflict, :value 4})
         #{1 2 3 4 5 6 7 8 9}))
  (is (= (cell-to-domain {:status :possible, :value #{1 2 3}})
         #{1 2 3})))

(deftest domain-to-cell-test
  (is (= (g/cell (domain-to-cell sudoku-grid [[3 1], #{1 2 3}]) 3 1)
         {:status :possible, :value #{1 2 3}}))
  (is (= (g/cell (domain-to-cell sudoku-grid [[1 1], #{1 2 3 5}]) 1 1)
         {:status :init, :value 5}))
  (is (let [grid (domain-to-cell sudoku-grid [[3 1], #{1 2 3 4}])]
        (= (g/cell (domain-to-cell grid [[3 1], #{4 5 6 1}]) 3 1)
           {:status :possible, :value #{1 4}})))
  (is (let [grid (domain-to-cell sudoku-grid [[3 1], #{1 2 3 4}])]
        (= (g/cell (domain-to-cell grid [[3 1], #{4 5 6}]) 3 1)
           {:status :solved, :value 4}))))

(deftest cols-to-doms-test
  (is (= (count (cols-to-doms sudoku-grid))
         9))
  (is (= (count (first (cols-to-doms sudoku-grid)))
         9))
  (is (= (get (first (cols-to-doms sudoku-grid)) [1 1])
         #{5}))
  (is (= (get (first (cols-to-doms sudoku-grid)) [1 3])
         #{1 2 3 4 5 6 7 8 9})))

(deftest rows-to-doms-test
  (is (= (count (rows-to-doms sudoku-grid))
         9))
  (is (= (count (first (rows-to-doms sudoku-grid)))
         9))
  (is (= (get (first (rows-to-doms sudoku-grid)) [1 1])
         #{5}))
  (is (= (get (first (rows-to-doms sudoku-grid)) [3 1])
         #{1 2 3 4 5 6 7 8 9})))

(deftest blocks-to-doms-test
  (is (= (count (blocks-to-doms sudoku-grid))
         9))
  (is (= (count (first (blocks-to-doms sudoku-grid)))
         9))
  (is (= (get (first (blocks-to-doms sudoku-grid)) [1 1])
         #{5}))
  (is (= (get (first (blocks-to-doms sudoku-grid)) [1 3])
         #{1 2 3 4 5 6 7 8 9})))

(deftest apply-alldiff-test
  (let [grid (apply-alldiff sudoku-grid cols-to-doms)
        x 3
        y 1]
       (is (= (g/cell grid x y)
              {:status :possible, :value #{1 2 3 4 5 6 7 9}})))
  (let [grid (apply-alldiff sudoku-grid rows-to-doms)
        x 3
        y 1]
       (is (= (g/cell grid x y)
              {:status :possible, :value #{1 2 4 6 8 9}})))
  (let [grid (apply-alldiff sudoku-grid blocks-to-doms)
        x 3
        y 1]
       (is (= (g/cell grid x y)
              {:status :possible, :value #{1 2 4 7}})))
  (is (= solved-grid (apply-alldiff solved-grid cols-to-doms)))
  (is (= solved-grid (apply-alldiff solved-grid rows-to-doms)))
  (is (= solved-grid (apply-alldiff solved-grid blocks-to-doms)))
  (is (= solved-grid (apply-alldiff partial-grid cols-to-doms))))

(deftest choose-var-test
  (is (= (choose-var (apply-alldiff sudoku-grid cols-to-doms))
         [[3 1] {:status :possible, :value #{1 2 3 4 5 6 7 9}}]))
  (let [grid (domain-to-cell (apply-alldiff sudoku-grid cols-to-doms) [[3 1], #{1 2 3}])]
    (is (= (choose-var grid)
         [[7 1] {:status :possible, :value #{1 3 4 5 6 7 8 9}}]))))

(deftest solve-test
  (is (= solved-grid (solve solved-grid)))
  (is (= solved-grid (solve partial-grid))))
