(ns mrsudoku.graph-test
    (:require [cljs.test :refer-macros [deftest testing is]]
              [mrsudoku.model.graph :refer [vars-of add-vertex add-edge
                                            remove-edge dfs-pre dfs-post
                                            transpose dfs-stack]]))
(def graph1 {:A #{:B, :C, :F}
             :B #{:C}
             :C #{:D}
             :D #{:E}
             :E #{:C}
             :F #{:C, :G, :H}
             :G #{:H, :I}
             :H #{:F, :I}
             :I #{}})

(def graph2 {:v1 #{1 2 3}
             :v2 #{1 2 4 5}
             :v3 #{4 5 6}
             :v4 #{4 5 6}
             :v5 #{4 5 6}})

(deftest vars-of-test
  (is (= (vars-of graph1)
         #{:A :B :C :D :E :F :G :H :I}))
  (is (= (vars-of graph2)
         #{:v1 :v2 :v3 :v4 :v5})))

(deftest add-vertex-test
  (is (= (add-vertex graph1 :A)
         graph1))
  (is (= (add-vertex graph1 :J)
       {:A #{:B, :C, :F}
        :B #{:C}
        :C #{:D}
        :D #{:E}
        :E #{:C}
        :F #{:C, :G, :H}
        :G #{:H, :I}
        :H #{:F, :I}
        :I #{}
        :J #{}})))

(deftest add-edge-test
  (is (= (add-edge graph2 :v1 4)
         {:v1 #{1 2 3 4}
          :v2 #{1 2 4 5}
          :v3 #{4 5 6}
          :v4 #{4 5 6}
          :v5 #{4 5 6}}))
  (is (= (add-edge graph2 :v0 4)
         {:v1 #{1 2 3}
          :v2 #{1 2 4 5}
          :v3 #{4 5 6}
          :v4 #{4 5 6}
          :v5 #{4 5 6}
          :v0 #{4}}))
  (is (= (add-edge graph2 :v1 1)
         graph2)))

(deftest remove-edge-test
  (is (= (remove-edge graph2 :v1 4)
         graph2))
  (is (= (remove-edge graph2 :v1 3)
         {:v1 #{1 2}
          :v2 #{1 2 4 5}
          :v3 #{4 5 6}
          :v4 #{4 5 6}
          :v5 #{4 5 6}}))
  (is (= (-> graph2
             (remove-edge :v3 4)
             (remove-edge :v3 5)
             (remove-edge :v3 6))
         {:v1 #{1 2 3}
          :v2 #{1 2 4 5}
          :v4 #{4 5 6}
          :v5 #{4 5 6}})))

(deftest dfs-pre-test
  (is (= (dfs-pre graph1 :B conj [])
         [:B :C :D :E]))
  (is (contains? #{[:F :C :D :E :G :H :I]
                   [:F :C :D :E :G :I :H]
                   [:F :C :D :E :H :I :G]
                   [:F :G :H :I :C :D :E]
                   [:F :G :I :H :C :D :E]
                   [:F :H :I :G :C :D :E]
                   [:F :H :I :C :D :E :G]}
       (dfs-pre graph1 :F conj []))
      (dfs-pre graph1 :F conj [])))

(deftest dfs-post-test
  (is (= (dfs-post graph1 :B conj [])
         [:E :D :C :B]))
  (is (contains? #{[:E :D :C :I :H :G :F]
                   [:I :H :G :E :D :C :F]}
        (dfs-post graph1 :F conj []))
      (dfs-post graph1 :F conj []))
  (is (contains? #{[:E :D :C :B :I :H :G :F :A] ;Toutes les sorties
                   [:E :D :C :I :H :G :F :B :A] ;possibles sont ici
                   [:I :H :E :D :C :G :F :B :A] ;Ssi on commence par :A
                   [:I :H :G :E :D :C :F :B :A]}
        (dfs-post graph1 :A conj []))
      (dfs-post graph1 :A conj [])))

(deftest transpose-test
  (is (= (transpose {}) {}))
  (is (= (transpose graph1)
         {:A #{}
          :B #{:A}
          :C #{:A :B :E :F}
          :D #{:C}
          :E #{:D}
          :F #{:A :H}
          :G #{:F}
          :H #{:F :G}
          :I #{:G :H}}))
  (is (= (transpose graph2)
         {1 #{:v1 :v2}
          2 #{:v1 :v2}
          3 #{:v1}
          4 #{:v2 :v3 :v4 :v5}
          5 #{:v2 :v3 :v4 :v5}
          6 #{:v3 :v4 :v5}
          :v1 #{}
          :v2 #{}
          :v3 #{}
          :v4 #{}
          :v5 #{}})))

(deftest dfs-stack-test
  (is (= (dfs-stack graph1)
         '(:A :B :F :G :H :C :D :E :I))))
;Certes c'est juste une seule valeur possible,
;mais là il y en a beaucoup trop pour toutes les écrire
