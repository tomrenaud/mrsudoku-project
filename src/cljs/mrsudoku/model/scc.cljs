(ns mrsudoku.model.scc
    (:require [mrsudoku.model.graph :as gr]))

(defn compute-scc
  "Produces a set of all scc in given graph
   'graph' should have the good arrows reversed already"
  [graph]
  (let [stack (gr/dfs-stack graph), tgraph (gr/transpose graph)]
    (loop [s stack, visited #{}, scc []]
      (if (seq s)
        (if (visited (first s))
          (recur (rest s) visited scc)
          (let [[comp, visited']
                (gr/dfs-post tgraph (first s) conj #{} visited)]
            (recur (rest s) visited' (conj scc comp))))
        scc))))

(defn doms-from-sccomp
  "Builds variable domains from given scc,
   with the set of all variables for reference."
  [variables scc]
  (if (= (count scc) 1)
    (if (contains? variables (first scc))
      {(first scc) #{}}
      {})
    ; non singular scc
    (let [vars (clojure.set/select #(contains? variables %) scc)
          values (clojure.set/difference scc vars)]
      (zipmap vars (repeat values)))))

(defn doms-from-scc
  "Uses the above function on all given scc, putting all results in the same map."
  [vars scc]
  (reduce (fn [res s] (conj res (doms-from-sccomp vars s)))
    {} scc))

(defn isolated-values
  "Returns all isolated values in given scc,
  with the set of all variables for reference."
  [vars scc]
  (into #{} (map first (filter
                         #(and (= (count %) 1) (not (contains? vars (first %))))
                        scc))))

(defn isolated-variables
  "Returns all isolated variables in given scc,
   with the set of all variables for reference."
  [vars scc]
  (into #{} (map first (filter
                         #(and (= (count %) 1) (contains? vars (first %)))
                        scc))))
